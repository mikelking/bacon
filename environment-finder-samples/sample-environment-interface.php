<?php

//require( __DIR__ . '/environment-finder.php' );

/**
 * Class WPE_Environment_Finder
 * An example of an implementation utilizing a incepted iterator
 */
class WPE_Environment_Interface implements Environment_Interface {
    private $clusters = array(
        'events' => array(
            'wp_hmevents'   => 'prod',
            'wp_hmestaging' => 'stage',
            'wp_devhme'     => 'dev',
        ),
        'bizmed' => array(
            'wp_haymarketwp' => 'prod',
            'wp_hmwpstaging' => 'stage',
            'wp_hmwpdev'     => 'dev',
        ),
        'bizmed-stg' => array(
            'wp_haymarketstage' => 'prod',
            'wp_hmsstage'       => 'stage',
            'hmsdev'            => 'dev',
        ),
        'local' => array(
            'events-cluster_lcl_development' => 'lcl',
            'example_com_development' => 'lcl',
        ),
    );

    public function findEnvironment() {
        $it = new RecursiveIteratorIterator( new RecursiveArrayIterator( $this->clusters ) );
        foreach ( $it as $key => $val ) {
            if ( $key === DB_NAME ) {
                return $val;
            }
        }
    }
}

/**
 * Need to convert this to a proper debugging construct
 *
// Simple logging to aid in troubleshooting
if ( class_exists( 'Singleton_Base' ) ) {
        $exception_msg = "NOTICE The current DB is => " . DB_NAME . ".";
        Singleton_Base::throw_exception_exception( $exception_msg );
}
*/
