<?php

/**
 * Class Plugin_Menu_base
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
class Plugin_Menu_base {
	const PAGE_TITLE      = 'BACON Tools Manager';
	const MENU_TITLE      = 'BACON Tools Manager';
	const MENU_SLUG       = 'bacon-tools-manager';
	const MENU_ICON       = 'dashicons-food';
	const MENU_POSITION   = 29;
	const USER_CAPABILITY = 'read';


	/**
	 * TAXONOMY_MENUS [ slug => title ]
	 */
	const TAXONOMY_MENUS  = [
		"category"                => "Categories",
		"post_tag"                => "Tags",
	];

	/**
	 * admin_menu - Implements the standard WordPress menu functionality. This should be
	 * refactored into an extendable mu-plugin.
	 * @return void
	 */
	public function admin_menu(): void {
		add_menu_page(
			static::PAGE_TITLE . ' Page',
			static::MENU_TITLE,
			static::USER_CAPABILITY,
			static::MENU_SLUG,
			null,
			static::MENU_ICON,
			static::MENU_POSITION
		);
		$this->add_taxonomy_menus();
		$this->add_custom_settings_menus();
	}

	/**
	 * add_custom_settings_menus - provides a simple method to override in the descendant classes for adding additional custom menu items.
	 * @return void
	 */
	public function add_custom_settings_menus(): void {
		return;
	}

	public function add_taxonomy_menus(): void {
		foreach ( static::TAXONOMY_MENUS as $slug => $title ) {
			$this->add_taxonomy_menu( $title, $slug );
		}
	}

	/**
	 * add_taxonomy_menu -
	 * @param $title
	 * @param $slug
	 * @return void
	 */
	public function add_taxonomy_menu( $title, $slug ): void {
		if ( taxonomy_exists ( $slug ) ) {
			add_submenu_page(
				static::MENU_SLUG,
				$title, // Menu Title
				$title, // Page Title
				static::USER_CAPABILITY,
				$this->get_taxonomy_menu_slug( $slug ),
				null
			);
		}
	}

	/**
	 * get_taxonomy_menu_slug - returns the properly formed taxonomy menu slug
	 * @param $taxonomy_slug
	 * @return string
	 */
	public function get_taxonomy_menu_slug( $taxonomy_slug ): string {
		return 'edit-tags.php?taxonomy=' . $taxonomy_slug;
	}
}
