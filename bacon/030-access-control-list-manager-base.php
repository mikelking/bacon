<?php
/*
Plugin Name:  Access Control List Manager
Version: 1.0.1
Description: This class should make it simple to create a manger for your various options in the CMS through an extensible means. Keep in mind that it is a basic first iteration and I shall endeavor to improve it over time.
Author: Mikel King
Text Domain: base-manager-admin
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

	Copyright (C) 2023, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:

		* Redistributions of source code must retain the above copyright notice, this
		list of conditions and the following disclaimer.

		* Redistributions in binary form must reproduce the above copyright notice,
		this list of conditions and the following disclaimer in the documentation
		and/or other materials provided with the distribution.

		* Neither the name of the {organization} nor the names of its
		contributors may be used to endorse or promote products derived from
		this software without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
	AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
	FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
	DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
	CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
	OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
	OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Access_Control_List_Base - it is up to the developer to properly instantiate
 * this class and call the appropriate permissions setter.
 *
 * Upon further review it may not be necessary to implement this at the post level because CPTs
 * follow a simple inheritance permissions scheme. Refer to the parent class constant CAPABILITY_TYPE
 * which is default set to post. This mean the new CPT will inherit the capabilities as aligned for all
 * roles relative to standard WordPress posts. You could change this to page or any other intrinsic post type.
 *
 * Each acl is stored in an array as follows:
 * [ 'role' =>
 *     'grant' => [
 *         'assign_terms' => 'assign_TERM',
 *     ],
 *     'deny' => [
 *         'edit_terms'   => 'edit_TERM',
 *         'manage_terms' => 'manage_TERM',
 *         'delete_terms' => 'delete_TERM',
 *     ]
 * ];
 *
 */
class Access_Control_List_Base {
    const GRANT = true;
    const DENY  = false;
	const DEBUG = false;

    const ADMIN_ROLES = [
        'administrator',
        'negadmin',
    ];
    const TERM_ACL_BASE = [
        'edit_terms'   => 'edit_',
        'manage_terms' => 'manage_',
        'delete_terms' => 'delete_',
        'assign_terms' => 'assign_',
    ];
    const POST_ACL_BASE = [
        'edit_post'          => 'edit_',
        'read_post'          => 'read_',
        'delete_post'        => 'delete_',
        'edit_posts'         => 'edit_',
        'edit_others_posts'  => 'edit_others_',
        'publish_posts'      => 'publish_',
        'read_private_posts' => 'read_private_',
        'create_posts'       => 'edit_',
    ];

	public $term_acls = [
		'edit_terms'   => 'edit_',
		'manage_terms' => 'manage_',
		'delete_terms' => 'delete_',
		'assign_terms' => 'assign_',
	];

    public $singular_name;
    public $name;
	public $acls = [];
	public $wpr;

	/**
	 * @param $singular_name
	 * @param $name
	 */
	public function __construct( $singular_name, $name ) {
		if ( !empty( $singular_name ) ) {
			$this->singular_name = strtolower( $singular_name );
		}

		if ( !empty( $name ) ) {
			$this->name = strtolower( $name );
		}
	}

    /**
     * set_admin_role_grants - setup grant acls for each administrator level role permission to do the new things
     * @param $base_acl
     * @return array
     */
    public function set_admin_role_grants( $base_acl ): array {
        $grants = [];

        foreach ( $base_acl as $key => $value ) {
          $grants[$key] = $value . $this->singular_name;
        }
        foreach ( static::ADMIN_ROLES as $admin ) {
            $acls[$admin] = [ 'grant' => $grants];
        }
		$this->acls = array_merge( $this->acls, $acls);
        return $this->acls;
    }

    /**
     * setup_post_items_capabilities - Accepts an array of access control lists related to post items
     * @param $acls
     * @return void
     */
    public function setup_post_items_capabilities( $acls ) {
		if ( is_array( $acls ) ) {
			$this->acls = array_merge( $this->acls, $acls );
		}
		if ( ! key_exists( 'administrator', $this->acls ) ) {
			$this->set_admin_role_grants( self::POST_ACL_BASE );
		}

		if ( static::DEBUG === true ) {
			var_dump( $this->acls );
		}
		$this->assign_item_acls();
    }

    /**
     * setup_taxonomy_items_capabilities - Accepts an array of access control lists related to taxonomy items
     * @param $acls
     * @return void
     */
    public function setup_taxonomy_items_capabilities( $acls ) {
		if ( is_array( $acls ) ) {
			$this->acls = array_merge( $this->acls, $acls );
		}

		if ( ! key_exists( 'administrator', $this->acls ) ) {
			$this->set_admin_role_grants( self::POST_ACL_BASE );
		}

		if ( static::DEBUG === true ) {
			var_dump( $this->acls );
		}

		$this->assign_item_acls();
    }

	public function assign_item_acls() {
		foreach ( $this->acls as $role => $acl ) {
			if ( key_exists( 'grant', $acl ) ) {
				$this->wpr = new WP_Roles();
				$this->set_item_grants( $role, $acl['grant']);
			} elseif ( key_exists( 'deny', $acl ) ) {
				$this->wpr = new WP_Roles();
				$this->set_item_denials( $role, $acl['deny']);
			}
		}
	}

	public function set_item_grants( $role, $grants ) {
		foreach ( $grants as $index => $capability ) {
			$this->wpr->add_cap( $role, $capability, self::GRANT );
		}
	}

	public function set_item_denials( $role, $denials ) {
		foreach ( $denials as $index => $capability ) {
			$this->wpr->add_cap( $role, $capability, self::DENY );
		}
	}

	/**
	 * get_wpr_connection - establishes the connection to the WordPress Roles object
	 * @return WP_Roles
	 */
	public function get_wpr_connection(): WP_Roles {
		$this->wpr = new WP_Roles();
		return $this->wpr;
	}

}
