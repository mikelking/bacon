<?php

/**
 * CPT_Custom_Field_Base - a factory for building custom fields related to this CPT
 */
class CPT_Custom_Field_Base {
	const EDIT_METABOX_CAPE = false;
	const TEXT_DOMAIN = 'text-domain';
	const SCREEN_TYPES = [ 'post' ];

	private $name;
	private $post;

	public $output;

	public function __construct( $name, $post_param = null ) {
		$this->get_post_data( $post_param );
		$this->name = $name;

		/* Fire our meta box setup function on the post editor screen. */
		//add_action( 'load-post.php', [ $this, "setup_metabox" ] );
		//add_action( 'load-post-new.php', [ $this, "setup_metabox" ] );
	}

	/**
	 * @param null $post_param
	 */
	private function get_post_data( $post_param = null ) {
		global $post;
		if ( ! isset( $post_param ) ) {
			$this->post = $post;
		} else {
			$this->post = $post_param;
		}
	}

	/**
	 * @param null $data
	 */
	public function setup_custom_field( $data = null ) {
		$default_data = '';

		if ( !isset( $data ) ) {
			$data = $default_data;
		}

		if ( ! isset( $this->post ) ) {
			$this->get_post_data();
		}

		if ( ! is_object( $this->post ) || ! property_exists( $this->post, 'ID' ) ) {
			return;
		}
		update_post_meta( $this->post->ID, $this->name, $data );
	}

	/**
	 * @param $post
	 */
	public function render_custom_field( $post ) {
		$field= [];
		$field['post'] = $post;
		$field['label'] = __( $this->name, static::TEXT_DOMAIN );
		$field['class_nonce'] = $field['label'] . '_class_nonce';
		$field['id'] = $this->name . '_' . static::SCREEN_TYPES[0];
		$field['data'] = esc_attr( get_post_meta( $post->ID, $this->name, true ) ) ?? "fakedata";

		wp_nonce_field( basename( __FILE__ ), $field['class_nonce'] );
		if ( static::EDIT_METABOX_CAPE ) {
			$this->output = $this->form_decorator( $field );
		} else {
			$this->output  = '<table class="form-table"><br />' . PHP_EOL;
			$this->output .= $this->table_row_decorator( $field );
			$this->output .= '</table><br />' . PHP_EOL;
		}

		return $this->output;
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function form_decorator( $field ): string {
		$output  = '<label for="' .  $field['id'] . '" >' . $field['label'] . '</label><br />' . PHP_EOL;
		$output .= '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $field['data'] . '" />' . PHP_EOL;
		return $output;
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function table_row_decorator( $field ): string {
		$output  = '<tr><br />' . PHP_EOL;
		$output .= '<td><label for="' .  $field['label'] . '" >' . sprintf( '<strong>%s</strong>', $field['label'] ) . '</label></td><br />' . PHP_EOL;
		$output .= '<td><br />';
		//$output .=  sprintf( '<p><strong>%s</strong></p>', $field['label'] ). PHP_EOL;
		$output .=  sprintf( '<p>%s</p>', $field['data'] ). PHP_EOL;
		//$output .=  sprintf( '<p>%s</p>', var_export( $field, true ) ). PHP_EOL;
		$output .= '</td><br />';
		$output .= '</tr><br />' . PHP_EOL;
		return $output;
	}
}
