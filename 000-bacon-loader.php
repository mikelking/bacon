<?php
/*
Plugin Name: Bacon Loader
Version: 1.0.0
Description: Adds a standardized registry pattern class for creating variants by extension
Author: Mikel King
Author URI: http://mikelking.com
License: BSD(3 Clause)
License URI: http://opensource.org/licenses/BSD-3-Clause

    Copyright (C) 2023, Mikel King, olivent.com, (mikel.king AT olivent DOT com)
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice, this
        list of conditions and the following disclaimer.

        * Redistributions in binary form must reproduce the above copyright notice,
        this list of conditions and the following disclaimer in the documentation
        and/or other materials provided with the distribution.

        * Neither the name of the {organization} nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
    OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * Bacon Autoloader
 * version 1.0.0
 */

class BaconLoader {
	const FILE_COUNT = 30;

	public $wp_base_path;
	public $source_path;
	public $iterator;
	public $current_path;
	public $wp_base = '015-wp-base.php';
	public array $file_list;

	public function __construct() {
		$this->wp_base_path = __DIR__ . '/local-bacon/';
		$this->source_path  = __DIR__ . '/bacon/';
		$this->file_list[]  = $this->wp_base;
		$this->current_path = basename( __FILE__, '.php' );
		$this->iterator = new DirectoryIterator( $this->source_path );
		$this->set_file_list();
		sort( $this->file_list );
		$this->require_files();
	}

	/**
	 * set_file_list - processes the output of the directory iterator against the target path
	 * to produce a list of files for processing
	 * @return void
	 */
	public function set_file_list(): void {
		while ( $this->iterator->valid() && $this->iterator->key() != self::FILE_COUNT ) {
			$file = $this->iterator->current();
			if ( ! $this->iterator->isDot() ) {
				$filename          = $file->getFilename();
				$this->file_list[] = $filename;
			}
			$this->iterator->next();
		}
	}

	/**
	 * require_files - processes the sorted file list to require each file into the system.
	 * @return void
	 */
	public function require_files(): void {
		foreach ( $this->file_list as $filename ) {
			//echo $filename . PHP_EOL;
			if ( $filename === $this->wp_base ) {
				$file = $this->wp_base_path . $filename;
			} else {
				$file = $this->source_path . $filename;
			}
			require( $file );
		}
	}
}

$bacon_loader = new BaconLoader();
